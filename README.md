# ProjectAr

Proyecto para construir una plataforma opensource de cursos online.

El proyecto surge para cubrir la necesidad que poseemos los desarrolladores y las personas realacionadas con el mundo tecnologico, para acceder a material de estudio en nuestro idioma.

 La idea general es que cualquier persona pueda subir y compartir su conocimiento con el resto de la comunidad y cobrar en pesos argentinos su curso, esto trae el beneficio que quienes compren su curso podran pagar en moneda local y se olvidaran de tener que pagar en dolares (con el consiguiente ahorro que esto implica),
 tambien facilita el cobro por parte del autor del material al cobrar directamente por mercadopago u otras vias validas en Argentina.

 El enfoque principal de la plataforma (ademas de crear una comunidad) es el de documentar en video paso a paso su creacion, ayudando a los desarrolladores
 que recien comienzan a tener una idea clara de como se trabaja en comunidad y en proyectos opensource, ademas de enseñarles distintas tecnologias y
 metodologias de desarrollo en el proceso.

 La plataforma sera creada por miembros de la comunidad de "Programadores Argentina", pero tambien puede sumarse quien este interesado en participar del proyecto.

Si tenes alguna idea que te huviese gustado ver en otras plataformas de educacion online, podes compartirla con nosotros en nuestro grupo de discord 
https://discord.gg/xCCuChBCaM en el canal ideas y asi la analizamos entre todos y la agregamos como un issue para hacer. Toda clase de ideas son bienvenidas!.

La propuesta completa esta aqui [propuesta](https://docs.google.com/document/d/1mnsx3PWfcsx7vtTrgLpuwnPUIUgRq8OegdSPsqfgc1A/edit?usp=sharing)
